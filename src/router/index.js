import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import paysmosmo from '@/components/paysmosmo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/paysmosmo',
      name: 'paysmosmo',
      component: paysmosmo
    },
    {
      path: '/',
      name: 'home',
      component: home
    }
  ]
})
