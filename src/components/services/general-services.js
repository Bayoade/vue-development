import axios from 'axios'
const token = 'ki494034jkjdfsi49e8943iufkljfdkj4oi3uu498usajhasdfoi4jkjfi434'

export class GeneralService {
  getAllCategory () {
    return axios.get(`https://demo4507124.mockable.io/categories`, {
      params: {
        'x-access-token': token
      }
    })
      .then(response => {
        return new Promise((resolve, reject) => resolve(response))
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  getAllProducts () {
    return axios.get(`https://demo4507124.mockable.io/products`, {
      params: {
        'x-access-token': token
      }
    })
      .then(response => {
        return new Promise((resolve, reject) => resolve(response))
      })
      .catch(function (error) {
        console.log(error)
      })
  }
}
